<?php
/**
 * @file
 * custom_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function custom_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'hi5_results_per_user';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'webform_submissions';
  $view->human_name = 'Hi5 results per user';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Hi5 results per user';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_tags'] = array(
    0 => 'ungroupsid',
  );
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'chart';
  $handler->display->display_options['style_options']['type'] = 'bar';
  $handler->display->display_options['style_options']['library'] = 'highcharts';
  $handler->display->display_options['style_options']['label_field'] = 'value_1';
  $handler->display->display_options['style_options']['data_fields'] = array(
    'data_1' => 'data_1',
    'value_1' => 0,
    'data' => 0,
  );
  $handler->display->display_options['style_options']['field_colors'] = array(
    'value_1' => '#2f7ed8',
    'data' => '#0d233a',
    'data_1' => '#8bbc21',
  );
  $handler->display->display_options['style_options']['legend_position'] = 'bottom';
  $handler->display->display_options['style_options']['width'] = '';
  $handler->display->display_options['style_options']['height'] = '';
  $handler->display->display_options['style_options']['xaxis_labels_rotation'] = '0';
  $handler->display->display_options['style_options']['yaxis_labels_rotation'] = '0';
  /* Relationship: Webform submissions: Data */
  $handler->display->display_options['relationships']['data']['id'] = 'data';
  $handler->display->display_options['relationships']['data']['table'] = 'webform_submissions';
  $handler->display->display_options['relationships']['data']['field'] = 'data';
  $handler->display->display_options['relationships']['data']['label'] = 'Submission Data Collegue';
  $handler->display->display_options['relationships']['data']['webform_nid'] = '2';
  $handler->display->display_options['relationships']['data']['webform_cid'] = '1';
  /* Relationship: Webform submissions: Data */
  $handler->display->display_options['relationships']['data_1']['id'] = 'data_1';
  $handler->display->display_options['relationships']['data_1']['table'] = 'webform_submissions';
  $handler->display->display_options['relationships']['data_1']['field'] = 'data';
  $handler->display->display_options['relationships']['data_1']['label'] = 'Submission Data - Category';
  $handler->display->display_options['relationships']['data_1']['webform_nid'] = '2';
  $handler->display->display_options['relationships']['data_1']['webform_cid'] = '3';
  /* Field: Webform submission data: Value */
  $handler->display->display_options['fields']['value_1']['id'] = 'value_1';
  $handler->display->display_options['fields']['value_1']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['value_1']['field'] = 'value';
  $handler->display->display_options['fields']['value_1']['label'] = 'Pick a category';
  $handler->display->display_options['fields']['value_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['value_1']['alter']['text'] = '[value_1]';
  $handler->display->display_options['fields']['value_1']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['value_1']['format'] = 'text';
  $handler->display->display_options['fields']['value_1']['webform_nid'] = '2';
  $handler->display->display_options['fields']['value_1']['webform_cid'] = '3';
  /* Field: Webform submission data: Value (raw) */
  $handler->display->display_options['fields']['data']['id'] = 'data';
  $handler->display->display_options['fields']['data']['table'] = 'webform_submitted_data';
  $handler->display->display_options['fields']['data']['field'] = 'data';
  $handler->display->display_options['fields']['data']['relationship'] = 'data_1';
  $handler->display->display_options['fields']['data']['exclude'] = TRUE;
  /* Field: COUNT(Webform submission data: Value (raw)) */
  $handler->display->display_options['fields']['data_1']['id'] = 'data_1';
  $handler->display->display_options['fields']['data_1']['table'] = 'webform_submitted_data';
  $handler->display->display_options['fields']['data_1']['field'] = 'data';
  $handler->display->display_options['fields']['data_1']['relationship'] = 'data';
  $handler->display->display_options['fields']['data_1']['group_type'] = 'count';
  $handler->display->display_options['fields']['data_1']['label'] = 'Value';
  /* Contextual filter: Webform submission data: Data field */
  $handler->display->display_options['arguments']['data']['id'] = 'data';
  $handler->display->display_options['arguments']['data']['table'] = 'webform_submitted_data';
  $handler->display->display_options['arguments']['data']['field'] = 'data';
  $handler->display->display_options['arguments']['data']['relationship'] = 'data';
  $handler->display->display_options['arguments']['data']['default_action'] = 'default';
  $handler->display->display_options['arguments']['data']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['data']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['data']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['data']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['data']['limit'] = '0';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['hi5_results_per_user'] = $view;

  $view = new view();
  $view->name = 'og_u_ref';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'OG U-ref';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: OG membership: OG Roles from membership */
  $handler->display->display_options['relationships']['og_users_roles']['id'] = 'og_users_roles';
  $handler->display->display_options['relationships']['og_users_roles']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_users_roles']['field'] = 'og_users_roles';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: User: Created date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'users';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: OG membership: Group ID */
  $handler->display->display_options['arguments']['gid']['id'] = 'gid';
  $handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['gid']['field'] = 'gid';
  $handler->display->display_options['arguments']['gid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['gid']['default_argument_type'] = 'og_user_groups';
  $handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['gid']['break_phrase'] = TRUE;
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: OG user roles: Role Name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'og_role';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'og_users_roles';
  $handler->display->display_options['filters']['name']['value'] = array(
    'Employee' => 'Employee',
  );
  $handler->display->display_options['filters']['name']['reduce_duplicates'] = TRUE;
  /* Filter criterion: User: Current */
  $handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['table'] = 'users';
  $handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['value'] = '0';

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'name' => 'name',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'ref';
  $export['og_u_ref'] = $view;

  $view = new view();
  $view->name = 'recognize_per_user';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'webform_submissions';
  $view->human_name = 'Recognize per user';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Recognize per user';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_tags'] = array(
    0 => 'ungroupsid',
  );
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'views_json';
  $handler->display->display_options['style_options']['root_object'] = '';
  $handler->display->display_options['style_options']['top_child_object'] = '';
  $handler->display->display_options['style_options']['plaintext_output'] = 1;
  $handler->display->display_options['style_options']['remove_newlines'] = 0;
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['style_options']['using_views_api_mode'] = 0;
  $handler->display->display_options['style_options']['object_arrays'] = 0;
  $handler->display->display_options['style_options']['numeric_strings'] = 0;
  $handler->display->display_options['style_options']['bigint_string'] = 0;
  $handler->display->display_options['style_options']['pretty_print'] = 0;
  $handler->display->display_options['style_options']['unescaped_slashes'] = 0;
  $handler->display->display_options['style_options']['unescaped_unicode'] = 0;
  $handler->display->display_options['style_options']['char_encoding'] = array();
  /* Relationship: Webform submissions: Data */
  $handler->display->display_options['relationships']['data']['id'] = 'data';
  $handler->display->display_options['relationships']['data']['table'] = 'webform_submissions';
  $handler->display->display_options['relationships']['data']['field'] = 'data';
  $handler->display->display_options['relationships']['data']['webform_nid'] = '5';
  $handler->display->display_options['relationships']['data']['webform_cid'] = '2';
  /* Relationship: Webform submissions: Data */
  $handler->display->display_options['relationships']['data_1']['id'] = 'data_1';
  $handler->display->display_options['relationships']['data_1']['table'] = 'webform_submissions';
  $handler->display->display_options['relationships']['data_1']['field'] = 'data';
  $handler->display->display_options['relationships']['data_1']['label'] = 'Submission Data - Pick a category';
  $handler->display->display_options['relationships']['data_1']['webform_nid'] = '5';
  $handler->display->display_options['relationships']['data_1']['webform_cid'] = '1';
  /* Relationship: Webform submissions: Data */
  $handler->display->display_options['relationships']['data_2']['id'] = 'data_2';
  $handler->display->display_options['relationships']['data_2']['table'] = 'webform_submissions';
  $handler->display->display_options['relationships']['data_2']['field'] = 'data';
  $handler->display->display_options['relationships']['data_2']['label'] = 'Submission Data - expectation';
  $handler->display->display_options['relationships']['data_2']['webform_nid'] = '5';
  $handler->display->display_options['relationships']['data_2']['webform_cid'] = '3';
  /* Field: Webform submission data: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['label'] = 'Category';
  $handler->display->display_options['fields']['value']['format'] = 'text';
  $handler->display->display_options['fields']['value']['custom_label'] = 'custom';
  $handler->display->display_options['fields']['value']['webform_nid'] = '5';
  $handler->display->display_options['fields']['value']['webform_cid'] = '1';
  /* Field: Webform submission data: Value (raw) */
  $handler->display->display_options['fields']['data']['id'] = 'data';
  $handler->display->display_options['fields']['data']['table'] = 'webform_submitted_data';
  $handler->display->display_options['fields']['data']['field'] = 'data';
  $handler->display->display_options['fields']['data']['relationship'] = 'data_1';
  $handler->display->display_options['fields']['data']['exclude'] = TRUE;
  /* Field: Webform submission data: Value (raw) */
  $handler->display->display_options['fields']['data_1']['id'] = 'data_1';
  $handler->display->display_options['fields']['data_1']['table'] = 'webform_submitted_data';
  $handler->display->display_options['fields']['data_1']['field'] = 'data';
  $handler->display->display_options['fields']['data_1']['relationship'] = 'data_2';
  $handler->display->display_options['fields']['data_1']['label'] = 'Type';
  /* Field: COUNT(Webform submission data: Value (raw)) */
  $handler->display->display_options['fields']['data_2']['id'] = 'data_2';
  $handler->display->display_options['fields']['data_2']['table'] = 'webform_submitted_data';
  $handler->display->display_options['fields']['data_2']['field'] = 'data';
  $handler->display->display_options['fields']['data_2']['relationship'] = 'data';
  $handler->display->display_options['fields']['data_2']['group_type'] = 'count';
  $handler->display->display_options['fields']['data_2']['label'] = 'Count';
  /* Contextual filter: Webform submission data: Data field */
  $handler->display->display_options['arguments']['data']['id'] = 'data';
  $handler->display->display_options['arguments']['data']['table'] = 'webform_submitted_data';
  $handler->display->display_options['arguments']['data']['field'] = 'data';
  $handler->display->display_options['arguments']['data']['relationship'] = 'data';
  $handler->display->display_options['arguments']['data']['default_action'] = 'default';
  $handler->display->display_options['arguments']['data']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['data']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['data']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['data']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['data']['limit'] = '0';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['recognize_per_user'] = $view;

  $view = new view();
  $view->name = 'submissions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'webform_submissions';
  $view->human_name = 'submissions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'submissions';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_tags'] = array(
    0 => 'ungroupsid',
  );
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'chart';
  $handler->display->display_options['style_options']['library'] = '';
  $handler->display->display_options['style_options']['label_field'] = 'value_1';
  $handler->display->display_options['style_options']['data_fields'] = array(
    'value' => 'value',
    'value_1' => 0,
    'data' => 0,
  );
  $handler->display->display_options['style_options']['field_colors'] = array(
    'value_1' => '#8bbc21',
    'data' => '#2f7ed8',
    'value' => '#0d233a',
  );
  $handler->display->display_options['style_options']['width'] = '';
  $handler->display->display_options['style_options']['height'] = '';
  $handler->display->display_options['style_options']['xaxis_labels_rotation'] = '0';
  $handler->display->display_options['style_options']['yaxis_labels_rotation'] = '0';
  /* Relationship: Webform submissions: Data */
  $handler->display->display_options['relationships']['data']['id'] = 'data';
  $handler->display->display_options['relationships']['data']['table'] = 'webform_submissions';
  $handler->display->display_options['relationships']['data']['field'] = 'data';
  $handler->display->display_options['relationships']['data']['webform_nid'] = '2';
  $handler->display->display_options['relationships']['data']['webform_cid'] = '1';
  /* Field: Webform submission data: Value */
  $handler->display->display_options['fields']['value_1']['id'] = 'value_1';
  $handler->display->display_options['fields']['value_1']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['value_1']['field'] = 'value';
  $handler->display->display_options['fields']['value_1']['label'] = 'Employee';
  $handler->display->display_options['fields']['value_1']['format'] = 'text';
  $handler->display->display_options['fields']['value_1']['webform_nid'] = '2';
  $handler->display->display_options['fields']['value_1']['webform_cid'] = '1';
  /* Field: Webform submission data: Value (raw) */
  $handler->display->display_options['fields']['data']['id'] = 'data';
  $handler->display->display_options['fields']['data']['table'] = 'webform_submitted_data';
  $handler->display->display_options['fields']['data']['field'] = 'data';
  $handler->display->display_options['fields']['data']['relationship'] = 'data';
  /* Field: COUNT(Webform submission data: Value) */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['group_type'] = 'count';
  $handler->display->display_options['fields']['value']['label'] = 'Reason';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'submissions';
  $export['submissions'] = $view;

  return $export;
}
