<?php
/**
 * @file
 * custom_pages_and_panes.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function custom_pages_and_panes_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'my_performance';
  $page->task = 'page';
  $page->admin_title = 'My Performance';
  $page->admin_description = '';
  $page->path = 'my-performance';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_my_performance__panel';
  $handler->task = 'page';
  $handler->subtask = 'my_performance';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1247baac-4dc5-4979-8053-5de5f7ae32f7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f2b8dacd-370b-4705-8b44-50e4886551e4';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'hi5_results_per_user';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => 'My Hi5s',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f2b8dacd-370b-4705-8b44-50e4886551e4';
    $display->content['new-f2b8dacd-370b-4705-8b44-50e4886551e4'] = $pane;
    $display->panels['middle'][0] = 'new-f2b8dacd-370b-4705-8b44-50e4886551e4';
    $pane = new stdClass();
    $pane->pid = 'new-89d517f9-bfc1-466b-8b7a-eb4d718e9ad0';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'My recognizes',
      'title' => '',
      'body' => '<?php 
echo custom_charts_recognize_per_user_chart();',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '89d517f9-bfc1-466b-8b7a-eb4d718e9ad0';
    $display->content['new-89d517f9-bfc1-466b-8b7a-eb4d718e9ad0'] = $pane;
    $display->panels['middle'][1] = 'new-89d517f9-bfc1-466b-8b7a-eb4d718e9ad0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-f2b8dacd-370b-4705-8b44-50e4886551e4';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['my_performance'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'recognize';
  $page->task = 'page';
  $page->admin_title = 'recognize';
  $page->admin_description = '';
  $page->path = 'recognize';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_recognize__panel';
  $handler->task = 'page';
  $handler->subtask = 'recognize';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'f25fc4df-bee2-40c7-9d73-a62ec50bf1b9';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0ea0638c-328d-4ce0-83a5-3da7bef4d765';
    $pane->panel = 'middle';
    $pane->type = 'node_title_and_type';
    $pane->subtype = 'node_title_and_type';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'node_title' => 'Recognize',
      'node_type' => 'webform',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0ea0638c-328d-4ce0-83a5-3da7bef4d765';
    $display->content['new-0ea0638c-328d-4ce0-83a5-3da7bef4d765'] = $pane;
    $display->panels['middle'][0] = 'new-0ea0638c-328d-4ce0-83a5-3da7bef4d765';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['recognize'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'send_hi5';
  $page->task = 'page';
  $page->admin_title = 'Send Hi5';
  $page->admin_description = '';
  $page->path = 'send-hi5';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_send_hi5__panel_context_c5605d69-9e0d-4b0b-a225-dc2e09f6e573';
  $handler->task = 'page';
  $handler->subtask = 'send_hi5';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'f25fc4df-bee2-40c7-9d73-a62ec50bf1b9';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2ec89177-5bf8-4263-8243-00a7959d2022';
    $pane->panel = 'middle';
    $pane->type = 'node_title_and_type';
    $pane->subtype = 'node_title_and_type';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'node_title' => 'Hi5 Form',
      'node_type' => 'webform',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2ec89177-5bf8-4263-8243-00a7959d2022';
    $display->content['new-2ec89177-5bf8-4263-8243-00a7959d2022'] = $pane;
    $display->panels['middle'][0] = 'new-2ec89177-5bf8-4263-8243-00a7959d2022';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['send_hi5'] = $page;

  return $pages;

}
