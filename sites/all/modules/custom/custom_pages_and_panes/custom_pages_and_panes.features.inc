<?php
/**
 * @file
 * custom_pages_and_panes.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function custom_pages_and_panes_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
