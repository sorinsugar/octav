<?php

/**
 * @file
 * Plugin to handle the 'node' content type which allows individual nodes
 * to be embedded into a panel.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Existing node by title and type'),
  'single' => TRUE,
  'defaults' => array(
    'node_title' => '',
    'node_type' => '',
    'links' => TRUE,
    'leave_node_title' => FALSE,
    'identifier' => '',
    'build_mode' => 'teaser',
  ),
  'icon' => 'icon_node.png',
  'description' => t('Add a node from your site as content.'),
  'category' => t('Custom'),
  'top level' => TRUE,
  'js' => array('misc/autocomplete.js'),
);

/**
 * Output function for the 'node' content type.
 *
 * Outputs a node based on the module and delta supplied in the configuration.
 */
function ctools_plugins_node_title_and_type_content_type_render($subtype, $conf, $panel_args) {
  $block = new stdClass();

  $node = ctools_plugins_get_node_by_title($conf['node_title'], $conf['node_type']);
  if (!$node || !node_access('view', $node)) {
    return;
  }

  // Don't store viewed node data on the node, this can mess up other
  // views of the node.
  $node = clone($node);

  $block->module = 'node';
  $block->delta = $node->nid;

  // Set block->title to the plain node title, then additionally set block->title_link to
  // the node url if required. The actual link is rendered in ctools_content_render().
  $block->title = check_plain($node->title);
  if (!empty($conf['link_node_title'])) {
    $block->title_link = 'node/' . $node->nid;
  }

  if (empty($conf['leave_node_title'])) {
    $node->title = NULL;
  }

  if (!empty($conf['identifier'])) {
    $node->ctools_template_identifier = $conf['identifier'];
  }

  // Handle existing configurations with the deprecated 'teaser' option.
  if (isset($conf['teaser'])) {
    $conf['build_mode'] = $conf['teaser'] ? 'teaser' : 'full';
  }

  $block->content = node_view($node, $conf['build_mode']);

  // Hide links if they've been suppressed.
  if (empty($conf['links'])) {
    $block->content['links']['#access'] = FALSE;
  }

  return $block;
}

/**
 * The form to add or edit a node as content.
 */
function ctools_plugins_node_title_and_type_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['leave_node_title'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($conf['leave_node_title']),
    '#title' => t('Leave node title'),
    '#description' => t('Advanced: if checked, do not touch the node title; this can cause the node title to appear twice unless your theme is aware of this.'),
  );

  $form['link_node_title'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($conf['link_node_title']),
    '#title' => t('Link the node title to the node'),
    '#description' => t('Check this box if you would like your pane title to link to the node.'),
  );

  if ($form_state['op'] == 'add') {
    $form['node_title'] = array(
      '#prefix' => '<div class="no-float">',
      '#title' => t('Enter the title of node'),
      '#description' => t('Enter node title'),
      '#type' => 'textfield',
      '#maxlength' => 512,
      '#autocomplete_path' => 'ctools/autocomplete/node',
      '#weight' => -10,
      '#suffix' => '</div>',
    );
    $form['node_type'] = array(
      '#prefix' => '<div class="no-float">',
      '#title' => t('Enter the type of node'),
      '#description' => t('Enter node type'),
      '#type' => 'select',
      '#options' => node_type_get_names(),
      '#weight' => -10,
      '#suffix' => '</div>',
    );
  }
  else {
    $form['node_title'] = array(
      '#type' => 'value',
      '#value' => $conf['node_title'],
    );
    $form['node_type'] = array(
      '#type' => 'value',
      '#value' => $conf['node_type'],
    );
  }

  $form['links'] = array(
    '#type' => 'checkbox',
    '#default_value' => $conf['links'],
    '#title' => t('Include node links for "add comment", "read more" etc.'),
  );

  $form['identifier'] = array(
    '#type' => 'textfield',
    '#default_value' => !empty($conf['identifier']) ? $conf['identifier'] : '',
    '#title' => t('Template identifier'),
    '#description' => t('This identifier will be added as a template suggestion to display this node: node--panel--IDENTIFIER.tpl.php. Please see the Drupal theming guide for information about template suggestions.'),
  );

  $entity = entity_get_info('node');
  $build_mode_options = array();
  foreach ($entity['view modes'] as $mode => $option) {
    $build_mode_options[$mode] = $option['label'];
  }

  // Handle existing configurations with the deprecated 'teaser' option.
  // Also remove the teaser key from the form_state.
  if (isset($conf['teaser']) || !isset($conf['build_mode'])) {
    unset($form_state['conf']['teaser']);
    $conf['build_mode'] = $conf['teaser'] ? 'teaser' : 'full';
  }
  $form['build_mode'] = array(
    '#title' => t('Build mode'),
    '#type' => 'select',
    '#description' => t('Select a build mode for this node.'),
    '#options' => $build_mode_options,
    '#default_value' => $conf['build_mode'],
  );
  return $form;
}

/**
 * Validate the node selection.
 */
function ctools_plugins_node_title_and_type_content_type_edit_form_validate(&$form, &$form_state) {
  $title = $form_state['values']['node_title'];
  $preg_matches = array();
  $match = preg_match('/.*?(?=\s\[id: (\d+)\])/', $title, $preg_matches);

  if ($match) {
    $title = $preg_matches[0];
  }
  if ($title) {
    $form_state['values']['node_title'] = $title;
  }
  else {
    form_error($form['node_title'], t('Invalid node title'));
  }
}

/**
 * Validate the node selection.
 */
function ctools_plugins_node_title_and_type_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('node_title', 'node_type', 'links', 'leave_node_title', 'link_node_title', 'identifier', 'build_mode') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title for a node.
 */
function ctools_plugins_node_title_and_type_content_type_admin_title($subtype, $conf) {
  $node = ctools_plugins_get_node_by_title($conf['node_title'], $conf['node_type']);
  if ($node) {
    if (!empty($node->status) || user_access('administer nodes')) {
      return check_plain($node->title);
    }
    else {
      return t('Unpublished node @nid', array('@nid' => $conf['nid']));
    }
  }
  else {
    return t('Deleted/missing node @nid', array('@nid' => $conf['nid']));
  }
}

/**
 * Display the administrative information for a node pane.
 */
function ctools_plugins_node_title_and_type_content_type_admin_info($subtype, $conf) {
  // Just render the node.
  return ctools_plugins_node_title_and_type_content_type_render($subtype, $conf, array());
}
